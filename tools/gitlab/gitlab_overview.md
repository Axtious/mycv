
# Gitlab

Gitlab est une plateforme permettant d'echanger les elements relatifs aux projets.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Installation Gitlab](#installation-gitlab)
- [Les Branches](#les-branches)
- [Gitignore](#gitignore)
- [Travailler à plusieurs avec Gitlab](#travailler-%C3%A0-plusieurs-avec-gitlab)
  - [La gestion des conflits](#la-gestion-des-conflits)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Repository= Dossier, projet.

## Installation Gitlab

1. Ouvrir le terminal
1. git init => Entrer
1. Aller sur le site `https://confluence.atlassian.com/bitbucket/configure-your-dvcs-username-for-commits-950301867.html`
1. Aller dans `Configure your Git username/email` et Copier les lignes **user name** et **user email** en veillant a mettre ses informations personnelles.
1. Copier les lignes et les coller sur le terminal puis valider. On valide chaque ligne a part.
1. Aller dans Webstorm => Preferences => Version control => Gitlad => Add new Gitlab server.
1. Remplir les chanps vides et le token. Ce dernier doit etre recupere depuis Gitlab dans Personnal Access Token => ok => ok.

## Les Branches

 Master: Version reference du projet validée par le chef de projetg.
 Branch: Copie du Master (Reference) à l'instqant T permettant d'apporter des modifications sur un projet de groupe.

 Afin d'apporter des modifications à un projet:

1. On cree une branche
1. On modifie le fichier sur lequel on travaille
1. On COMMIT/push
1. On genere une Merge request
1. Les COMMIT sont envoyés au responsable du projet qui le bascule en master.

## Gitignore

 Il sert à ignorer les dossiers/fichiers qu'on ne voudrait pas rendre public et faire apparaitre sur Gitlab tels les node-modules.
 Pour l'installer, il suffir de créer un fichier `gitignore` et rajouter les fichiers/dossiers qu'on veut ignorer en les rajoutant au fichier `gitignore`.

  A noter:

>l'ideal est de créé un Gitignore juste aprés git init. Les dossiers à ignorer se mettront ainsi automatiquement dans gitignore.

## Travailler à plusieurs avec Gitlab

Pour travaller à plusierus sur un projet, on utilise le principe des branches.

Les merges request  doivent etre reponsables soit utiles et respectueuse de l’environnement.

### La gestion des conflits

Quand plusieurs personnes travaillent sur le meme dossier, il, peut arriver que deux personnes modifient la meme ligne sans se concerter commit/push/Merge request.

Au moment de merger, la personne chargé de valider les pull request dera confrontée à des conflits en examinants les pulls request de chacun des deux collaborateurs.

En effet, les deux collaborateurs ayant modifié la meme élement, il ne lui est plus possible de valider la merge. Il validera donc la premiére et refusera la deuxième en demandant au collaborateur qui a commité en dernier de se conformer.

Les conflits peuvent etre resolus sur le git ou sur l’IDE.
Lors de la resolution des conflits, les collaborateurs concernés doivent se mettre d’accord sur les élèmnts à garder et ceux à écarter.
