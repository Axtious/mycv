<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Put](#put)
  - [Exemple d'utilisation](#exemple-dutilisation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Put

Il s'agit d'une méthode qui permet de remplacer une ressource par une autre.
## Exemple d'utilisation

``` javascript 

import express from 'express';
import bodyParser from "body-parser";

const app = express();

app.use(bodyParser({}));
let users = [
    {
    id:1,
    firstName: 'toto',
    age: 20,
},
{
    id:2,
    firstName: 'titi',
    age: 12,
    car: true,
},
{
    id:3,
    firstName: 'tete',
    age: 12,
},
    ];
app.put('/api/users/:id',
(req, res) => {
const newUser= req.body;
const id = parseInt(req.params.id);
const index= users
     .findIndex((user)=>user.id === id);
users[index]= {...newUser,id};
return res.send(users);
});
``` 

Sur le debug `Postman`, on selectionne la méthode Put. Au niveau de l'api,on rajoute l'index 2 après users.
`http://localhost:3015/api/users/2`.

Cela veut dire qu'on va remplacer la ressource dont l'id est le 2 par une autre ressource comme suit:

![img](tools/nodejs/images/put1.png) 

Le résultat final :

![img](tools/nodejs/images/put2.png)
