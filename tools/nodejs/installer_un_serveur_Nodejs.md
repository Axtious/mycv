<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Installer Node.js sur Mac](#installer-nodejs-sur-mac)
  - [Installer brew](#installer-brew)
  - [Installer Node.js avec brew](#installer-nodejs-avec-brew)
  - [Installer npm](#installer-npm)
  - [Installer npm concurently](#installer-npm-concurently)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Installer Node.js sur Mac
Pour installer `Node.js`sur MacOs, on utilise `Brew`.
## Installer brew

Pout installer brew, il faudrait:
- Aller sur le terminal et saisir la commande suivante:

```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"```

- Saisir la commade `brew --Version`
## Installer Node.js avec brew
- Saisir sur le terminal la commande `brew install node`.
- Ensuite `node --version`.
## Installer npm
- Saisir la commande `npm install` afin d'installer les `node_modules`.
## Installer npm concurently
il permet d'exécuter plusieurs commandes en même temps. pour ce, on saisit la commade `npm install -g concurrently
`.
