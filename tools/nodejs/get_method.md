<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Get](#get)
  - [Exemple d'utilisation](#exemple-dutilisation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Get
Get est une méthode qui sert à chercher une ressource.
## Exemple d'utilisation

``` javascript
const users =  {
    id:1,
    firstName: 'toto',
    age: 20,
};
app.get('/api/user', (req, res) => {
    return res.send(user);
});
```
Sur le debug Postman, on sélectionne la méthode `Get`, `Send` et on obtient le résultat suivant :

![img](tools/nodejs/images/get1.png)
