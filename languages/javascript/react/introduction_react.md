<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [QU'est-ce-que React ?](#quest-ce-que-react-)
  - [Les componants:](#les-componants)
    - [Les props et les states](#les-props-et-les-states)
      - [Les props:](#les-props)
      - [Les states:](#les-states)
  - [le DOM virtuel:](#le-dom-virtuel)
  - [Les hooks](#les-hooks)
    - [useState:](#usestate)
    - [useEffet:](#useeffet)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Qu'est-ce-que React ?
Réact est une bibliothéque front-end Javascript crée en 2013 par facebook.
React est principalement basé sur une logique centrée sur les composants ou components . React dans son approche centrée “Composants” suggère de rassembler le HTML, le JS et le CSS au sein du composant et rendre cela lisible via le JSX (Javascript XML). ReactJS implémente un DOM Virtuel, il effectue les opérations sur ce dernier puis compare celui-ci au vrai DOM pour voir quelles parties de l’application doivent être modifiées.
## Les componants:
Un `component` implémentent une méthode `render()` qui prend des données en entrée et retourne ce qui doit être affiché.

```JavaScript
class SayingHey extends React.Component {
  render() {
    return (
      <div>
        Hey {this.props.name}
      </div>
    );
  }
}

ReactDOM.render(
  <SayingHello name="Taous" />,
  document.getElementById('hey-example')
);
```

### Les props et les states

#### Les props:

 Un props permet au component de récupérer.

#### Les states:

Un state permet de stocker localement un état. on y accéde via `this.state` lorsqu'on utilise des classe et `useState` lorsau'on utilise les hooks.

## le DOM virtuel:

Un DOM Virtuel est une représentation du DOM en JavaScript. Il permet de faire une copie du DOM (Document Object Model) permettant ainsi d'apporter des modification aux composants de façon efficace et optimale.

## Les hooks
Les hooks ont été introduits par la version React 16.8. Ils représentent une alternative aux classes en utilisant des foncions (programmation fonctionnelle).

### useState:

Il permet d'avoir un `state` dans une fonction .

```JavaScript
const MyHookFunction = (props) => {
     let [name, setName] = useState('');
        return (
            <input type="text" onChange={(event) => setName(event.target.value)}></input>
            <span>Bonjour, je m'appelle {name}</span>
        )
};
```
### useEffet:

Il nous permet de combiner 3 lifecycle hooks de React, `componentDidMount`, `componentDidUpdate` et `componentWillUnmount`.
Ainsi, useEffect permet de  récupérer des données (call Api), mettre à jour des variables ou encore faire des modifications sur le DOM.

```JavaScript
const App = () => {
  const [width, setWidth] = useState(0)
  const [height, setHeight] = useState(0)

  const onResize = event => {
    const { innerWidth, innerHeight } = event.target
    setWidth(innerWidth)
    setHeight(innerHeight)
  }

  useEffect(() => {
    setWidth(window.innerWidth)
    setHeight(window.innerHeight)
    window.addEventListener('resize', onResize)
    return () => {
      window.removeEventListener('resize', onResize)
    }
  })

  return (
    <span>
      {width}x{height}
    </span>
  )
}
```
