<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Math.round()](#mathround)
- [Math.floor()](#mathfloor)
- [Math.random()](#mathrandom)
- [Éléments complémentaires](#%C3%A9l%C3%A9ments-compl%C3%A9mentaires)
  - [min,max](#minmax)
  - [toFixed()](#tofixed)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

En JavaScript, maths est une méthode qui nous permet d'effectuer des opérations sur des nombres.

## Math.round()

Cette méthode permet d'arrondir un nombre àl'entier le plus proche.
> Exemple
> ``` JavaScript
> Math.round(2.49);
> Math.round(2.50);
> ```

Résultat

```
2
3
```

## Math.floor()

Cette méthode sert à ôter ce qui vient après la virgule ( tel un Slice).
> Exemple
>```JavaScript
> Math.floor(2,49);
> ```

Résultat

```
2
```

## Math.random()

Cette méthode renvoie un nombre aléatoire compris entre 0 et 1.
> Exemple
> ```JavaScript
>console.log(Math.random()*1000 +1);
>```
> Résultat
>```
> 99 (un nombre aléatoire entre 1 et 1000)
>```

A noter: pour ne pas avoir de nombre à virgule, on rajouter la méthode `Math floor()` comme dans l'exemple qui suit :

>```JavaScript
> console.log(Math.floor(Math.random()*100+1));
> ```

Le résultat serait un nombre aléatoire entier entre 1 et 1000.

## Éléments complémentaires

### min,max

afin de générer un nombre aléatoire compris entre un maximum et un minimum on applique la méthode `random()` comme suit :

> ```JavaScript
> function getValue(min,max){
>   return Math.floor(min + Math.random() * (max - min) + 1 )
> }
>console.log(getValue(10,15));
>```

Si on veut inclure le `max`:

> ```JavaScript
>function getValue(min,max){
> return Math.floor(min+max.Math.randon()*(max-min)+1);
>console.log(getValue(10,15));
>```

### toFixed()

cette méthode sert à fixer le nombre de chiffres après la virgule.
> Exemple
>
>
> ```Javascript
> toFixed(3)
> ```
>
> Résultat
> Le résultat serait un nombre avec 3 chiffres après la virgule.

`toFixed()` renvoie une string. pour avoir un type `number` on ajoute la méthode `parseInt()` pour les nombre entiers et `parseFloat()` pour les nombres décimaux.
