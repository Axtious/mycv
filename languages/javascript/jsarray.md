# Array specifities

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Array specifities](#array-specifities)
  - [Array destructuration](#array-destructuration)
  - [Object destructuration](#object-destructuration)
  - [Object attribute access](#object-attribute-access)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Array destructuration

Le but de la destructuration est de décomposer un tableau en plusieurs variables comme suit :

```JavaScript
const tab = ['white', 'green', 'yellow', 'grey'];
const [var1,var2,var3,var4] = tab;
console.log(var2);
```

 > Résultat
 >
 > green

 On peut également faire comme suit:

 ```JavaScript
const tab = ['white', 'green', 'yellow', 'grey'];
const [,var2,,] = tab;
console.log(var2);
```

 > Résultat
 >
 > green

## Object destructuration

Le but de la destructuration des objets est de travailler sur une ou plusieurs propriétés de l'objet en les stockant dans une variable. On y procède comme suit:

```JavaScript
const person = {
    firstName : 'Baby'
    lastName: 'Shark'
    Age: 1
    Adress: '5 Ocean street'
};

const {firstName, lastName} = person;
console.log(firstName, lastName);
```

> Résultat
>
> Baby  
> Shark

## Object attribute access

Le but est d'accéder à l'attribut d'un objet lorsqu'on ignore le nom de celui-ci. Regardons l'exemple ci-dessous:

```JavaScript
const person = {
    firstName : 'Baby'
    lastName: 'Shark'
    Age : 1
    Adress: 5 Ocean street

};
```

Pour accéder aux attributs first name, last name et age on procède comme suit :

```JavaScript
const attrs = ['firstName','lastName','age'];
attrs.forEach(attr => comnsole.log (person[attr]));
```

> Résultat  
> Baby  
> Shark  
> 1
