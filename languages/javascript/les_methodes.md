
# Les méthodes
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Foreach](#foreach)
- [Map](#map)
- [Filter](#filter)
- [Sort](#sort)
- [Splice](#splice)
  - [Pour supprimer un élément](#pour-supprimer-un-%C3%A9l%C3%A9ment)
  - [pour rajouter un élément](#pour-rajouter-un-%C3%A9l%C3%A9ment)
  - [pour rajouter un élément et en supprimer un autre](#pour-rajouter-un-%C3%A9l%C3%A9ment-et-en-supprimer-un-autre)
- [Slice](#slice)
- [Some](#some)
- [Every](#every)
- [Find](#find)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Foreach

Cette méthode permet de parcourir un tableau et d'exécuter une fonction donnée sur chacun de ses éléments.
    Exemple

```JavaScript
const hello = ['box', 'dish', 'spoon'];
hello.forEach(thing => {
  console.log(thing);
});
```

> ```bash
>  Résultat
>box
>dish
>spoon

## Map

Tout comme `foreach()`, cette méthode permet de parcourir les éléments d’un tableau mais également de créer un nouveau tableau avec les résultats d’appel d'une fonction fournie sur chaque élément du tableau appelant.

Le JS est un langage de passage par référence. ceci veut dire que quand on manipule un élément, on va manipuler l’objet stocké dans la mémoire de l’ordinateur.

l’utilisation de map modifie le tableau d'origine  mémoire  mais également le nouveau tableau.
pour éviter cela, on crée une variable qu'on appelle par exemple `temp`  et fabrique un nouvel objet comme sur l'exemple ci-dessous.

Exemple

```JavaScript
const tab2 = tab .map(user:{…} | …, index: number) => {
       const temp = {…user};
 temp.index = index
 Return temp;
 Console.table(tab);
 Console.table(tab2);
```

## Filter

Cette méthode crée et renvoie un nouveau tableau contenant tous les éléments du tableau initial qui remplissent une condition déterminée par la fonction callback.

Exemple

```JavaScript
const test = [ 'chocolat', 'lait', 'pain', 'blé',  'savon' ];
const Rtest = test.filter(test => test.length > 4);
console.log(test);

```

> ```bash
>  Résultat
> ['Chocolat', 'savon']
> ```

## Sort

Il permet de  trier les éléments d'un tableau, dans ce même tableau, et renvoie le tableau.

Exemple

```JavaScript

const seasons = ['hiver', 'printemps', 'été', 'automne'];
months.sort();
console.log(months);

```

> ```bash
>  Résultat
> ['automne', 'été', 'hiver', 'printemps']
> ```

## Splice

 Cette méthode permet de modifier un array en supprimant ou ajoutant de nouveau éléments.

 Elle permets ainsi de vider ou remplcer un ou pludieurs élememnts du tableau.

 Exemples

### Pour supprimer un élément

```JavaScript
let thing = [ 'jupe', 'pantalon', 'chemise', 'robe' ];
console.log(thing.splice(1,1));
```

> ```bash
>  Résultat
> Jupe, chemise,robe
> ```

### pour rajouter un élément

```JavaScript
let thing = [ ‘jupe', ‘pantalon', ‘chemise', ‘robe];
console.log(thing.splice(1,0,chaussure));
```

> ```bash
>  Résultat
> ['Jupe','chaussure','pantalon','chemise','robe']
> ```

### pour rajouter un élément et en supprimer un autre

```JavaScript
let thing = [ ‘jupe', ‘pantalon', ‘chemise', ‘robe];
console.log(thing.splice(2,1,chaussure));
```

> ```bash
>  Résultat
> ['Jupe','pantalon','chaussure','robe']
> ```

## Slice

cette méthode de JavaScript permet de renvoyer une partie du tableau en découpant une portion de ce dernier. la portion est définie par un index qui un un début et une fin, la fin n’étant pas incluse dans le nouveau tableau renvoyé.

Slice ne modifie pas le tableau d’origine.

Exemple

```JavaScript

Const toto = [ ‘jupe', ‘pantalon', ‘chemise', ‘robe’];
console.log (toto.slice(1,2))
```

> ```bash
>  Résultat
> ['jupe','pantalon']
> ```

Cette méthode permet de parcourir un tableau et d’accumuler l’ensemble des valeurs d’un élément de gauche à droite pour les réduire en une seule valeur.

Exemple

```JavaScript

const numbers = [20,30,40,50];
console.log(numbers);

```

> ```bash
>  Résultat
> 140
> ```

## Some

Cette méthode permet de tester si au moins un élément du tableau correspond aux conditions imposées par la fonction fournie. Elle renvoie un booléen soit `true` ou `false` indiquant ainsi le résultat de l'opération.

Utilisée sur un tableau vide, `some` renverra `false` quelque soit la condition de départ.

Exemple

```JavaScript
function estAssezGrand(element, index, array) {
  return element >= 10;
 const toto = [22, 7, 3, 180, 14].some(estAssezGrand);
 ```

> ```bash
>  Résultat
> True

## Every

Cette méthode permet de tester si tous les élément du tableau correspondent aux conditions imposées par la fonction fournie. Elle renvoie un booléen soit `true` ou `false` indiquant ainsi le résultat de l'opération.

Utilisée sur un tableau vide, `every` renverra `false` quelque soit la condition de départ.

Exemple

```JavaScript
function estAssezGrand(element, index, array) {
  return element >= 10;
 const toto = [22, 7, 3, 180, 14].every(estAssezGrand);
 ```

> ```bash
>  Résultat
> False

## Find

Cette méthode renvoie le premier élément du tableau correspondant aux conditions de la fonction.

```JavaScript
const firstarray = [10, 19, 23, 27, 39];
const toto = firstarray.find(element => element > 20);
console.log(toto);
 ```

> ```bash
>  Résultat
>23
