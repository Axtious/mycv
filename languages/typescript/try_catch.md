# la gestion des exceptions avec Try...Catch 

Try...Catch est une instruction qui permet de définir un bloc d'instructions qu'on essaye  d'exécuter, ainsi qu'une ou plusieurs instructions à utiliser en cas d'erreur quand une exception se présente.  

Si une exception est signalée, l'instruction try...catch permettra de la localiser et de définir ce qui se passe dans ce cas.

L'instruction try...catch se compose de deux parties:
-  Un bloc try qui contient une ou plusieurs instructions. 
- Un bloc catch qui contient les instructions à exécuter quand une exception se produit dans le bloc try.

Exemple 

``` javascript
app.get('/api/hostels/:hotelId/rooms', (req, res) => {
    try {
        return manageRoomsToGet(req, res, hostels);
    } catch (e) {
        return res.status(500).send(e.message);
    }
});
```