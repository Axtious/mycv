<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [TAOUS DENEUVE](#taous-deneuve)
  - [Formations](#formations)
  - [Experiences professionnelles](#experiences-professionnelles)
  - [Divers](#divers)
  - [Langues](#langues)
  - [Liens](#liens)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# TAOUS DENEUVE

Actuellement en formation pour devenir developpeuse JS

## Formations

* **2020**: _Formation Developpeur JS_.

  * _White Rabbit_.
* **2015**: _Formation gestionnaire paie_.
  * _Synopsis Paie_.
* **2013**: _Master Tourisme_.
  * _UVSQ_

## Experiences professionnelles

* **2015-2016**: Gestionnaire paie.
  * Cabinet RSM
* **2016-2017**: Gestionnaire paie.
  * Cabinet SAADI

* **2017-2018**: Gestionnaire paie.
  * Societe CPM
* **2010-2013**: Manager en restauration rapide.

## Divers

* Logiciels paie:
SILAE SAGE HRPATH

## Langues

| Langue | Niveau |
|------|-----------|
| Anglais | Intermediaire |
| Français | Bilingue |

## Liens

[Linkedin](https://fr.linkedin.com/in/taousdeneuve)
